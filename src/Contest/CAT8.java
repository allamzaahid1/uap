package Contest;

class CAT8 extends TiketKonser {
    //Do your magic here...

    //Membuat konstruktor untuk dimasukkan kedalam switch case pada pemilihan jenis tiket dalam class main
    public CAT8() {

        /*
        Menggunakan super untuk memanggil konstruktor kelas induk,
        konstruktor dari kelas induk "TiketKonser" diisi jenis tiket serta harga tiket sebagai detail tiket
        */
        super("CAT8", 100000);
    }
}