package Contest;

class FESTIVAL extends TiketKonser {
    //Do your magic here...

    //Membuat konstruktor untuk dimasukkan kedalam switch case pada pemilihan jenis tiket dalam class main
    public FESTIVAL() {

        /*
        Menggunakan super untuk memanggil konstruktor kelas induk,
        konstruktor dari kelas induk "TiketKonser" diisi jenis tiket serta harga tiket sebagai detail tiket
        */
        super("FESTIVAL", 300000);
    }
}