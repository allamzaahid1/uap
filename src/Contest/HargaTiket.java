package Contest;

interface HargaTiket {
    //Do your magic here...
    
    /*
    Class interface untuk menentukan agar class yang mengimplementasikannya harus memiliki method yang telah di deklarasi,
    Dalam kasus ini class TiketKonser harus memiliki method yang telah di deklarasi yaitu getHarga()
    */
    int getHarga();
}