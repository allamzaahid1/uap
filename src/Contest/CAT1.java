package Contest;

class CAT1 extends TiketKonser {
    //Do your magic here...

    //Membuat konstruktor untuk dimasukkan kedalam switch case pada pemilihan jenis tiket dalam class main
    public CAT1() {

        /*
        Menggunakan super untuk memanggil konstruktor kelas induk,
        konstruktor dari kelas induk "TiketKonser" diisi jenis tiket serta harga tiket sebagai detail tiket
        */
        super("CAT1", 200000);
    }
}