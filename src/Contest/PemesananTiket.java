package Contest;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

class PemesananTiket {
    // Do your magic here...

    //Deklarasi variable yang akan dijadikan detail dari pemesanan tiket
    private String namaPemesan;
    private TiketKonser tiket;
    private String kodePesanan;
    private String tanggalPesanan;
    private int totalHarga;

    /*
    Membuat konstruktor untuk menginputkan nama pemesan dan jenis tiketnya
    Serta memasukkan kode pesanan dan tanggal dari class main, dan harga tiket
    */
    public PemesananTiket(String nama, TiketKonser tiket) {
        this.namaPemesan = nama;
        this.tiket = tiket;
        this.kodePesanan = Main.generateKodeBooking();
        this.tanggalPesanan = Main.getCurrentDate();
        this.totalHarga = tiket.getHarga();
    }

    //Membuat method untuk menampilkan detail pesanan
    public void cetakDetailPesanan() {

        //Menatur DecimalFormat menjadi rupiah
        DecimalFormat Rupiah = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols IDR = new DecimalFormatSymbols();
        IDR.setCurrencySymbol("Rp ");
        IDR.setMonetaryDecimalSeparator('.');
        IDR.setGroupingSeparator(',');

        System.out.println();
        System.out.println("----- Detail Pesanan -----");
        System.out.println("Nama Pemesan: " + namaPemesan);
        System.out.println("Kode Pesanan: " + kodePesanan);
        System.out.println("Tanggal Pesanan: " + tanggalPesanan);
        System.out.println("Nama Tiket: " + tiket.getNamaTiket());

        //Menggunakan DecimalFormat untuk menampilkan TotalHarga menjadi rupiah
        Rupiah.setDecimalFormatSymbols(IDR);
        System.out.printf("Total Harga: %s %n", Rupiah.format(totalHarga));

    }
}