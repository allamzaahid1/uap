package Contest;

abstract class TiketKonser implements HargaTiket {
    // Do your magic here...

    //Deklarasi variabel dari jenis tiket dan harga tiket
    private String jenisTiket;
    private int hargaTiket;

    //Membuat konstruktor untuk menginputkan jenis tiket dan harga tiket 
    public TiketKonser(String jenis, int harga) {
        this.jenisTiket = jenis;
        this.hargaTiket = harga;
    }

    //Membuat method getNamaTiket() untuk mendapatkan nama tiket
    public String getNamaTiket() {
        return jenisTiket;
    }

    //Membuat method getHarga() untuk mendapatkan harga tiket serta sebagai persyaratan dari interface HargaTiket
    public int getHarga() {
        return hargaTiket;
    }
}