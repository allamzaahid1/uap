/*
 * UAP PEMLAN 2023
 * DURASI: 120 MENIT
 * TEMPAT PENGERJAAN: DARING/LURING
 * =================================================================
 * Semangat mengerjakan UAP teman-teman
 * Jangan lupa berdoa untuk hasil yang terbaik
 */

package Contest;

import java.util.*;
import java.text.SimpleDateFormat;

public class Main {
    public static void main(String[] args) {
        //Do your magic here...

        //Membuat scanner untuk input pengguna
        Scanner scanner = new Scanner(System.in);

        //Membuat input nama pengguna untuk memesan
        System.out.print("Masukkan nama pemesan: ");
        String namaPemesan = scanner.nextLine();

        //Menampilkan Jenis-jenis tiket
        System.out.println("Jenis Tiket:");
        System.out.println("1. CAT8");
        System.out.println("2. CAT1");
        System.out.println("3. FESTIVAL");
        System.out.println("4. VIP");
        System.out.println("5. UNLIMITED EXPERIENCE");
        System.out.print("Masukkan pilihan: ");

        //Deklarasi integer yang nanti akan digunakan untuk pilihan pengguna
        int jenisTiket = 0;

        //Menggunakan boolean untuk menentukan apakan input sudah benar
        boolean validInput = false;


        while (!validInput) {
            
            //Menggunakan try untuk mengecek input, dan ketika terjadi ketidak sesuaian akan menampilkan pesan error
            try {

                //Scanner untuk memasukkan nilai integer yang digunakan untuk memilih tiket
                jenisTiket = scanner.nextInt();
            
                //Agar program tidak berhenti setelah menggunakan scanner untuk integer
                scanner.nextLine();

                /*
                Menggunakan if untuk mengecek apakah pengguna sudah memasukkan input dengan benar (1-5)
                Jika pengguna memasukkan nilai dibawah 1 dan diatas 5 akan menampilkan pesan error
                */
                if (jenisTiket < 1 || jenisTiket > 5) {

                    //Menggunakan throw untuk menandai kondisi input yang tidak valid dan menghentikan program.
                    throw new InvalidInputException("Terjadi kesalahan, Silakan pilih jenis tiket menggunakan angka 1 sampai 5");
                }

                validInput = true;
            } 
        
            /*
            Menggunakan catch untuk menampilkan pesan error, 
            Menggunakan NumberFormatException agar ketika pengguna menginputkan nilai selain integer akan menampilkan pesan error
            */
            catch (InputMismatchException e) {
                System.out.println("Terjadi kesalahan, Silahkan pilih jenis tiket menggunakan angka bulat 1 sampai 5");
                return;
            }

            /*
            Menggunakan catch untuk menampilkan pesan error, 
            Memanggil class InvalidInputException agar ketika pengguna menginputkan nilai yang tidak valid akan menampilkan pesan error
            */
            catch (InvalidInputException e) {
                System.out.println(e.getMessage());
            }
        }

        //Deklarasi "tiket" sebagai jenis tiket konser menggunakan class abstract TiketKonser sebagai tipe datanya
        TiketKonser tiket;

        /*
        Memasukkan nilai integer yang telah diinputkan oleh pengguna kedalam switch case untuk memilih jenis tiket,
        Tiap case berisikan code untuk memasukkan jenis tiket kedalam variabel "tiket" yang tadi telah dideklarasi
        dengan cara memanggil konstruktor yang telah dibuat dari class jenis-jenis tiket yang ada kedalam "tiket" lalu menggunakan break untuk mengakhiri operasi case
        */
        switch (jenisTiket) {
            case 1:
                tiket = new CAT8();
                break;
            case 2:
                tiket = new CAT1();
                break;
            case 3:
                tiket = new FESTIVAL();
                break;
            case 4:
                tiket = new VIP();
                break;
            case 5:
                tiket = new VVIP();
                break;
            default:
                tiket = null;
        }

        /*
        Menggunakan if untuk mengecek apakah variabel "tiket" sudah terisi dengan jenis tiket
        Jika variabel "tiket" masih kosong akan menampilkan pesan error
        */
        if (tiket == null) {
            System.out.println("Terjadi kesalahan saat memilih jenis tiket.");
            return;
        }

        //Memanggil class PemesananTiket untuk melakukan pesanan dan memasukkan data terkait tiketnya
        PemesananTiket pemesanan = new PemesananTiket(namaPemesan, tiket);

        //Menampilkan detail dari pemesanan yang telah dilakukan
        pemesanan.cetakDetailPesanan();
    
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan kode pesanan atau kode booking
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String generateKodeBooking() {
        StringBuilder sb = new StringBuilder();
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int length = 8;

        for (int i = 0; i < length; i++) {
            int index = (int) (Math.random() * characters.length());
            sb.append(characters.charAt(index));
        }

        return sb.toString();
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan waktu terkini
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = new Date();
        return dateFormat.format(currentDate);
    }
}